var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

var radius = 5;
var drag = false;
let currentTool = 'brush';
var usingBrush = false;
var PushArray = new Array();
var step = 0;

canvas.width = 800;
canvas.height = 500;

context.lineWidth = radius * 2;

canvas.style.cursor = "url('image/brush.png') 0 32, auto";

PushArray[0] = context.getImageData(0, 0, canvas.width, canvas.height);

function ChangeTool(toolClicked){
    if(toolClicked == 'brush'){
        canvas.style.cursor = "url('image/brush.png') 0 32, auto";
    }else if(toolClicked == 'eraser'){
        canvas.style.cursor = "url('image/eraser.png') 0 32, auto";
    }else if(toolClicked == 'text'){
        canvas.style.cursor = "url('image/type.png') 0 32, auto";
    }else if(toolClicked == 'circle'){
        canvas.style.cursor = "url('image/circle.png') 0 32, auto";
    }else if(toolClicked == 'triangle'){
        canvas.style.cursor = "url('image/triangle.png') 0 32, auto";
    }else if(toolClicked == 'rectangle'){
        canvas.style.cursor = "url('image/rectangle.png') 0 32, auto";
    }else if(toolClicked == 'line'){
        canvas.style.cursor = "url('image/line.png') 0 32, auto";
    }
    currentTool = toolClicked;
}

function GetPosition(x, y) {
    let canvasSize = canvas.getBoundingClientRect();
    return {
        x: (x - canvasSize.left) * (canvas.width / canvasSize.width),
        y: (y - canvasSize.top) * (canvas.height / canvasSize.height)
    };
}

function SaveCanvasImage() {
    savedImageData = context.getImageData(0, 0, canvas.width, canvas.height);
}

function RedrawCanvasImage() {
    context.putImageData(savedImageData, 0, 0);
}

function Push(){
    step++;
    if(step < PushArray.length){
        PushArray.length = step;
    }
    PushArray.push(context.getImageData(0, 0, canvas.width, canvas.height));
}

function Undo(){
    if(step > 0){
        step--;
            context.putImageData(PushArray[step], 0, 0);
    }
}

function Redo(){
    if(step < PushArray.length-1){
        step++;

            context.putImageData(PushArray[step], 0, 0);
    
    }
}

function refresh(){
    step = 0;
    context.clearRect(0, 0, canvas.width, canvas.height);
    SaveCanvasImage();
}
