var fillColor;

canvas.addEventListener("mousedown", ReactToMouseDown);
canvas.addEventListener("mousemove", ReactToMouseMove);
canvas.addEventListener("mouseup", ReactToMouseup);


class ShapeBoundingBox {
    constructor(left, top, width, height) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }
}

class MouseDownPos {
    constructor(x, y) {
        this.x = x,
            this.y = y;
    }
}

class Location {
    constructor(x, y) {
        this.x = x,
            this.y = y;
    }
}

class TrianglePoint {
    constructor(x, y) {
        this.x = x,
            this.y = y;
    }
}

let shapeBoundingBox = new ShapeBoundingBox(0, 0, 0, 0);
let mousedown = new MouseDownPos(0, 0);
let loc = new Location(0, 0);

function UpdateRubberbandSizeData(loc) {
    shapeBoundingBox.width = Math.abs(loc.x - mousedown.x);
    shapeBoundingBox.height = Math.abs(loc.y - mousedown.y);

    if (loc.x > mousedown.x) {
        shapeBoundingBox.left = mousedown.x;
    } else {
        shapeBoundingBox.left = loc.x;
    }

    if (loc.y > mousedown.y) {
        shapeBoundingBox.top = mousedown.y;
    } else {
        shapeBoundingBox.top = loc.y;
    }
}

function getAngleUsingXAndY(mouselocX, mouselocY) {
    let adjacent = mousedown.x - mouselocX;
    let opposite = mousedown.y - mouselocY;
    return radiansToDegrees(Math.atan2(opposite, adjacent));
}

function radiansToDegrees(rad) {
    if (rad < 0) {
        return (360.0 + (rad * (180 / Math.PI))).toFixed(2);
    } else {
        return (rad * (180 / Math.PI)).toFixed(2);
    }
}

function DegreesToRadians(degrees) {
    return degrees * (Math.PI / 180);
}

function getTrianglePoints() {
    let angle = DegreesToRadians(getAngleUsingXAndY(loc.x, loc.y));
    let radiusX = shapeBoundingBox.width;
    let radiusY = shapeBoundingBox.height;
    let trianglePoints = [];

    for (let i = 0; i < 3; i++) {
        trianglePoints.push(new TrianglePoint(loc.x + radiusX * Math.sin(angle)
            , loc.y - radiusY * Math.cos(angle)));
        angle += 2 * Math.PI / 3;
    }
    return trianglePoints;
}

function getTriangle() {
    let trianglePoints = getTrianglePoints();
    context.beginPath();
    context.moveTo(trianglePoints[0].x, trianglePoints[0].y);
    for (let i = 0; i < 3; i++) {
        context.lineTo(trianglePoints[i].x, trianglePoints[i].y);
    }
    context.closePath();
}

function drawRubberbandShape(loc) {
    context.strokeStyle = strokeColor;
    context.fillStyle = fillColor;

    if (currentTool === "brush") {
        usingBrush = true;
    } else if (currentTool === "line") {
        context.beginPath();
        context.moveTo(mousedown.x, mousedown.y);
        context.lineTo(loc.x, loc.y);
        context.stroke();
    } else if (currentTool === "rectangle") {
        context.strokeRect(shapeBoundingBox.left, shapeBoundingBox.top,
            shapeBoundingBox.width, shapeBoundingBox.width);
    } else if (currentTool === "circle") {
        canvas.style.cursor = "url('image/circle.png') 0 32, auto";
        let radius = shapeBoundingBox.width;
        context.beginPath();
        context.arc(mousedown.x, mousedown.y, radius, 0, Math.PI * 2);
        context.stroke();
    } else if (currentTool === "triangle") {
        canvas.style.cursor = "url('image/triangle.png') 0 32, auto";
        getTriangle();
        context.stroke();
    }
}

function UpdateRubberbandOnMove(loc) {
    UpdateRubberbandSizeData(loc);
    drawRubberbandShape(loc);
    //usingbrush = false;
}

function ReactToMouseDown(e) {
    context.globalCompositeOperation = "source-over";
    loc = GetPosition(e.clientX, e.clientY);

    SaveCanvasImage();
    mousedown.x = loc.x;
    mousedown.y = loc.y;
    drag = true;
    if (currentTool == 'brush' || currentTool == 'eraser') {
        if (currentTool == 'eraser') {
            context.globalCompositeOperation = "destination-out";
        }
        context.lineWidth = radius * 2;
        usingBrush = true;
        drag = true;
        context.lineTo(e.clientX - 50, e.clientY - 50);
        context.stroke();
        context.beginPath();
        context.arc(e.clientX - 50, e.clientY - 50, radius, 0, Math.PI * 2);
        context.fill();
        context.beginPath();
        context.moveTo(e.clientX - 50, e.clientY - 50);
    }

}

function ReactToMouseMove(e) {
    context.globalCompositeOperation = "source-over";
    loc = GetPosition(e.clientX, e.clientY);
    if ((currentTool == 'brush' || currentTool == 'eraser') && drag && usingBrush) {
        //RedrawCanvasImage();
        if (currentTool == 'eraser') {
            context.globalCompositeOperation = "destination-out";
        }
        context.lineTo(e.clientX - 50, e.clientY - 50);
        context.stroke();
        context.beginPath();
        context.arc(e.clientX - 50, e.clientY - 50, radius, 0, Math.PI * 2);
        context.fill();
        context.beginPath();
        context.moveTo(e.clientX - 50, e.clientY - 50);
    } else {
        if (drag) {
            RedrawCanvasImage();
            UpdateRubberbandOnMove(loc);
        }
    }

}

function ReactToMouseup(e) {
    context.globalCompositeOperation = "source-over";
    loc = GetPosition(e.clientX, e.clientY);
    //RedrawCanvasImage();
    UpdateRubberbandOnMove(loc);
    drag = false;
    usingbrush = false;
    if (currentTool == 'eraser') {
        context.globalCompositeOperation = "destination-out";
    }
    context.beginPath();
    Push();
}

/*
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

var drag = false;
var dragStartLocation;
var spot;

function getCanvasCoordinates(e) {
    var x = event.clientX - canvas.getBoundingClientRect().left,
        y = event.clientY - canvas.getBoundingClientRect().top;

    return {x: x, y: y};
}

function remeber_the_spot(){
    spot = context.getImageData(0,0, canvas.width, canvas.height);
}

function restore_the_spot(){
    context.putImageData(spot, 0, 0);
}

function circle(position){
    canvas.style.cursor = "url('image/circle.png') 0 32, auto";canvas.style.cursor = "url('image/circle.png') 0 32, auto";
    var radius = Math.sqrt(Math.pow((dragStartLocation.x - position.x), 2) + Math.pow((dragStartLocation.y - position.y), 2));
    context.beginPath();
    context.arc(dragStartLocation.x, dragStartLocation.y, radius, 0, 2*Math.PI, false);
}
/*
function dragStart(e) {
    drag = true;
    dragStart = getCanvasCoordinates(e);
    remeber_the_spot();
}

function dragging(e){
    var position;
    if(drag == true){
        restore_the_spot();
        position = getCanvasCoordinates(e);
    }
}

function dragStop(e){
    drag = false;
    restore_the_spot();
    var position = getCanvasCoordinates(e);
}


canvas.addEventListener('mousedown', dragStart);
canvas.addEventListener('mousemove', dragging);
canvas.addEventListener('mouseup', dragStop);

var putPoint = function (e) {
    if (drag) {
        restore_the_spot();
        position = getCanvasCoordinates(e);
        context.lineTo(e.clientX-50, e.clientY-50);
        context.stroke();
        context.beginPath();
        context.arc(e.clientX-50, e.clientY-50, radius, 0, Math.PI * 2);
        context.fill();
        context.beginPath();
        context.moveTo(e.clientX-50, e.clientY-50);
    }
}

var engage = function (e) {
    drag = true;
    dragStartLocation =getCanvasCoordinates(e);
    remember_the_spot();
}

var disengage = function () {
    drag = false;
    restore_the_spot();
    var position = getCanvasCoordinates(e);
}

canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', putPoint);
canvas.addEventListener('mouseup', disengage);
*/